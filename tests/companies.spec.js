import { test, expect } from '@playwright/test'
import url from './url'
import CompaniesPage from './pages/Companies'
import ArticlePage from './pages/Article'

test('open companies page', async ({ page }) => {
  await page.goto(url.companies)
  const companiesPage = new CompaniesPage(page)
  await expect(companiesPage.storiesFeed).toBeVisible()
})

test('collapse article', async ({ page }) => {
  await page.goto(url.companies)
  const companiesPage = new CompaniesPage(page)
  const firstArticle = await companiesPage.getArticleByOrder(0)
  await firstArticle.collapseArticle()
  await expect(firstArticle.article).toHaveClass(/story_collapse/)
})

// Тест который иногда проходит, а иногда вмеесто смены рейтинга предлагает залогинится и падает. Не знаю баг или фича.
test('activate rating buttons', async ({ page }) => {
  await page.goto(url.companies)
  const companiesPage = new CompaniesPage(page)
  const firstArticle = await companiesPage.getArticleByOrder(0)
  await firstArticle.increaseRating()
  await firstArticle.ratingUpIcon.waitFor()
  await expect(firstArticle.ratingUpIcon).toHaveClass(/story__rating-up_active/)
})

test('article dots menu with duplicate report option', async ({ page }) => {
  await page.goto(url.companies)
  const companiesPage = new CompaniesPage(page)
  const firstArticle = await companiesPage.getArticleByOrder(0)
  await firstArticle.headerDotsMenu.click()
  await expect(page.locator('.popup.popup_show .popup-menu_dots-menu > div')).toHaveAttribute('data-id', 'duplicate_report')
})

test('redirect on title click', async ({ page }) => {
  await page.goto(url.companies)
  const companiesPage = new CompaniesPage(page)
  const firstArticle = await companiesPage.getArticleByOrder(0)
  const newTabPromise = page.waitForEvent('popup');
  await firstArticle.articleTitle.click()
  const newTab = await newTabPromise
  const articlePage = new ArticlePage(newTab)
  await expect(await articlePage.articleComponent.article).toBeVisible()
})
