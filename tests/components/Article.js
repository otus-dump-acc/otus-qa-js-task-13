const locators = {
  ratingUpButton: '.story__rating-plus',
  ratingUpIcon: '.story__rating-up',
  ratingDownButton: '.story__rating-down',
  collapseButton: '.story__left > .story__scroll > .collapse-button',
  authorPanel: '.story__author-panel',
  subscribeAuthorPanelButton: '.button-group',
  articleTitle: '.story__main > .story__header > .story__title',
  headerDotsMenu: '.story__main > .story__header > .story__dots',
}

export default class Article {
  article
  ratingUpButton
  ratingUpIcon
  ratingDownButton
  collapseButton
  authorPanel
  subscribeAuthorPanelButton
  articleTitle
  headerDotsMenu


  constructor(articleLocator) {
    this.article = articleLocator
    this.ratingUpButton = this.article.locator(locators.ratingUpButton)
    this.ratingUpIcon = this.ratingUpButton.locator(locators.ratingUpIcon)
    this.ratingDownButton = this.article.locator(locators.ratingDownButton)
    this.collapseButton = this.article.locator(locators.collapseButton)
    this.authorPanel = this.article.locator(locators.authorPanel)
    this.subscribeAuthorPanelButton = this.authorPanel.locator(locators.subscribeAuthorPanelButton)
    this.articleTitle = this.article.locator(locators.articleTitle)
    this.headerDotsMenu = this.article.locator(locators.headerDotsMenu)
  }

  async openArticle() {
    await this.articleTitle.click()
    return this
  }

  async collapseArticle() {
    await this.collapseButton.click()
    return this
  }

  async increaseRating() {
    await this.ratingUpButton.click()
    return this
  }

  async decreaseRating() {
    await this.ratingDownButton()
    return this
  }

  async subscribeByAuthorPanelButton() {
    await this.subscribeAuthorPanelButton.click()
    return this
  }
}