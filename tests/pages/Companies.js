import Article from "../components/Article"

const locators = {
  storiesFeed: '.stories-feed',
  article: '.stories-feed__container > .story'
}

export default class CompaniesPage {
  page
  storiesFeed
  storiesList

  constructor(page) {
    this.page = page
    this.storiesFeed = page.locator(locators.storiesFeed)
    this.storiesList = page.locator(locators.article).all()
  }

  async getArticleByOrder(orderNumber) {
    return new Article((await this.storiesList)[orderNumber])
  }
}