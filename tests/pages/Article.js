import Article from "../components/Article"

const locators = {
  article: 'div.page-story__story > article'
}

export default class ArticlePage {
  page
  articleComponent

  constructor(page) {
    this.page = page
    this.articleComponent = new Article(page.locator(locators.article))
  }

}